libs = poppler-cpp

CXXFLAGS = -std=c++20 $(shell pkg-config --cflags $(libs)) -g
LDLIBS = $(shell pkg-config --libs $(libs))

all: pdfcheck
