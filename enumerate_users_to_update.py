#!/usr/bin/env python3

"""
Extraherar registrerade medlemsavgifter från GnuCASH.

Ansluter till en GnuCASH-fil i SQLite-format, och listar alla
transaktioner från kontot "Medlemsavgifter". Om en transaktion är ett
nummer följt av ett *nix-användarnamn så skris information hur
användarens medlemsskap skall uppdateras. Annars skris transaktionen
ut med en varning.
"""

import sqlite3
import re
from datetime import date, datetime, timedelta
from getpass import getpass
from medreg.api import auth as medreg_auth
from medreg.api import Session as MedregSession
import sys
from dataclasses import dataclass
from abc import ABC
from typing import Literal
import pwd
import os
from argparse import ArgumentParser
import urllib3
import logging
from math import floor

urllib3.disable_warnings()

logger = logging.getLogger(__name__)

logger.setLevel(logging.DEBUG)

LYSATOR_MEMBERSHIP_FEE = 256
LOGIN_URL = "https://login.lysator.liu.se/auth/realms/Lysator/protocol/openid-connect/token"  # noqa: E501
API_BASE_URL = "https://medreg.lysator.liu.se/api/"
CLIENT_ID = "medreg"  # "User Agent"


def gnucash_connect(path: str) -> sqlite3.Cursor:
    """
    Opens a new database connection to a GNUCash file.

    @param path
        An absolute path to a GNUCash file using the SQLite backend.

    @return
        An SQLite cursor object.
    """
    con = sqlite3.connect(f"file:{path}?mode=ro", uri=True)
    return con.cursor()


def gnucash_payments_since(cur: sqlite3.Cursor,
                           account: str,
                           since: date) -> list[tuple[float, date, str]]:
    """
    Get all transactions on account since a given date.

    @param cur
        A cursor connected to a GNUCash SQLite file.
    @param account
        Name of the GNUCash money account, such as "Membership Fees"
        or "Coffe expenses"
    @param since
        How far back the query should go. Transactions from the date
        in question are NOT included.
    @return
        A list of tuples, each tuple containing
        * the monetary value of the transaction, in "whole" values
          (e.g. a float with cents in the decimals)
        * The date of the transaction
        * the note left by the accountant for that transaction.
    """
    # TODO document how split transactions are handled
    cur.execute("""
        SELECT - s.value_num / s.value_denom AS `kr`
             , date(tx.post_date) AS `post_date`
             , tx.description AS `description`
        FROM splits s
        LEFT JOIN accounts a ON s.account_guid = a.guid
        LEFT JOIN transactions tx ON s.tx_guid = tx.guid
        WHERE a.name = ?
          AND post_date > ?
        ORDER BY `post_date` ASC
        """, (account, since.isoformat()))

    return [(mon, date.fromisoformat(dt), desc) for (mon, dt, desc) in cur]


@dataclass
class Operation(ABC):
    """
    Base class for operations.

    An operation is something to be done with a user, such as creating
    a new one, or updating their membership time. Each operation has
    its source somewhere in the GNUCash file.

    @param at
        The transaction date from GNUCash.
    """
    at: date


@dataclass
class AddOperation(Operation):
    """
    Creation of a new user.

    @param user
        The wanted name of the new user.
    """
    user: str


@dataclass
class ExtendOperation(Operation):
    """
    Extension of an existing membership.

    Before creation the `username` and `user_id` values should be
    checked to correspond to each other.

    @param username
        Name of the user to extend
    @param user_id
        User ID of the user to extend.
    @param years
        Number of years the user has paid for.
    """
    username: str
    user_id: int
    years: int


@dataclass
class WeirdExtendOperation(Operation):
    """
    Weird extensions to existing memberships.

    The user has paid to extend their membership, but not in an even
    increment the membership fee.
    """
    username: str
    user_id: int
    money: int


@dataclass
class MiscOperation(Operation):
    """Other operations."""
    msg: str


def days_paid_for(money: int) -> int:
    """
    Returns the number of days:s
    """
    return floor(365 * (money / LYSATOR_MEMBERSHIP_FEE))


def determine_operations(entries: list[tuple[float, date, str]]) \
        -> list[Operation]:
    # Regex matching a user-id, followed by a username.
    rx = r'(\d+) ([a-zA-Z0-9_][a-zA-Z0-9_-]*[$]?)'

    ops: list[Operation] = []

    for (kr, d, description) in entries:
        if kr == 50:
            ops.append(AddOperation(at=d, user=description))

        elif m := re.match(rx, description):
            username = m[2]
            user_id = m[1]

            if kr % LYSATOR_MEMBERSHIP_FEE != 0:
                ops.append(WeirdExtendOperation(at=d,
                                                username=username,
                                                user_id=int(user_id),
                                                money=int(kr)))
            else:
                # Validate that UID and username match
                # matches = ession.api_searchbynumber(int(m[1]))
                # assert len(matches) == 1
                # assert matches[0]['login'] == m[2]
                ops.append(ExtendOperation(
                    at=d,
                    username=username,
                    user_id=int(user_id),
                    years=int(kr//LYSATOR_MEMBERSHIP_FEE)))

        else:
            ops.append(MiscOperation(at=d, msg=f"Unhandled: {description}"))

    return ops


def validate_user(session: MedregSession,
                  username: str, user_id: int) -> str | Literal[False]:
    """
    Check medreg if the username and user_id are fine.

    This checks that there is exactly one user with the given id, and
    that the given id maps to the given username.

    @return
        On error a human readable string describing the error is returned,
        if everything is fine False is returned.
    """
    result = session.api_searchbynumber(user_id)
    if not result:
        return f"No user with id = {user_id} ({username})"
    if len(result) > 1:
        return f"Multiple users with id = {user_id} ({username})"
    user = result[0]

    if user['login'] != username:
        return f"Username and user_id doesn't match: {user_id}, {username}"

    return False


def sort_ops_into_buckets(medreg: MedregSession,
                          operations: list[Operation]) \
                                  -> tuple[list[AddOperation],
                                           list[ExtendOperation],
                                           list[MiscOperation]]:

    # Users to be created.
    # This will be a manual process since the source data is
    # elsewhere.
    to_create: list[AddOperation] = []

    # Users to extend membership for, where the payment and data looks
    # correct. This can be executed automatically.
    to_extend: list[ExtendOperation] = []

    # Operations which are weird in some way. And needs to be checked
    # manually.
    to_check: list[MiscOperation] = []

    for op in operations:
        match op:
            case AddOperation():
                # TODO check that this user doesn't already exist?
                to_create.append(op)

            case ExtendOperation(at=at, username=name, user_id=id, years=y):
                if err := validate_user(medreg, name, id):
                    msg = f"{err}, but paid for {y} year(s)"
                    to_check.append(MiscOperation(at=at, msg=msg))
                else:
                    to_extend.append(op)

            case WeirdExtendOperation(at=at,
                                      username=name,
                                      user_id=id,
                                      money=kr):
                if err := validate_user(medreg, name, id):
                    msg = f"{err}, and paid {kr} kr"
                    to_check.append(MiscOperation(at=at, msg=msg))

                else:
                    info = medreg.api_searchbynumber(id)[0]
                    expire = date.fromisoformat(info['expdate'])
                    days = days_paid_for(kr)
                    end = expire + timedelta(days=days)
                    msg = f"Extend {name} ({id}) to {end}"
                    to_check.append(MiscOperation(at=at, msg=msg))

            case MiscOperation():
                to_check.append(op)

            case _:
                logger.error("Got unexpected operation: %s", op)

    return (to_create, to_extend, to_check)


@dataclass
class LoginFailure(BaseException):
    error: str
    description: str


def setup_session(username: str, password: str) -> MedregSession:
    login = medreg_auth(login_url=LOGIN_URL,
                        client_id=CLIENT_ID,
                        usr=username,
                        passwd=password)

    print(login)

    if 'error' in login:
        raise LoginFailure(error=login['error'],
                           description=login.get('error_description',
                                                 "Unknown error"))

    token = login['access_token']

    return MedregSession(api_base_url=API_BASE_URL,
                         token=token,
                         verify_cert=True)


class Operations(ABC):
    def __init__(self, session: MedregSession):
        self.session = session

    def handle_extend_operations(self, ops: list[ExtendOperation]) -> None:
        ...

    def handle_create_operations(self, ops: list[AddOperation]) -> None:
        ...

    def handle_check_operations(self, ops: list[MiscOperation]) -> None:
        ...


class DryOperations(Operations):
    def handle_extend_operations(self, ops: list[ExtendOperation]) -> None:
        for op in ops:
            msg = f"Extending membership for {op.username} " \
                  f"({op.user_id}) by {op.years} years"
            print(msg)

    def handle_create_operations(self, ops: list[AddOperation]) -> None:
        for op in ops:
            print(f"You have to create {op.user}")

    def handle_check_operations(self, ops: list[MiscOperation]) -> None:
        for op in ops:
            print("You have to check", op)


class RealOperations(DryOperations):
    def handle_extend_operations(self, ops: list[ExtendOperation]) -> None:
        for op in ops:
            logger.info(f"Registering payment for {op.username}")
            self.session.api_registerpayment(op.user_id, op.years)


def build_argparse() -> ArgumentParser:
    parser = ArgumentParser(description="Update Lysators member database from GnuCash.")  # noqa: E501
    parser.add_argument(
            '--force',
            help='actually do the thing',
            action='store_true')
    parser.add_argument(
            '--file',
            help='source GnuCash file',
            default='/lysator/styrelsen/2023-2024/kassor/bokforing_2023_2024.gnucash',   # noqa: E501
            action='store')
    parser.add_argument(
            '--account',
            help='account in GnuCash to get data from',
            default='Medlemsavgifter',
            action='store')
    parser.add_argument(
            '--since',
            help='date to check since, not inclusive. Defaluts to what the server says',  # noqa: E501
            type=date.fromisoformat,
            action='store')

    return parser


def prompt_login(username: str) -> MedregSession:
    """Prompt for password, and attempt setting up session until success."""

    while True:
        password = getpass(f"Password for {username}:")

        try:
            return setup_session(username, password)
        except LoginFailure as e:
            print()
            print(f"Failed logging in: {e.description}")


def main() -> int:
    args = build_argparse().parse_args()

    file = args.file
    account_name = args.account

    username = pwd.getpwuid(os.getuid()).pw_name
    medreg_session = prompt_login(username)

    last_payment: date
    if args.since:
        last_payment = args.since
    else:
        dt = medreg_session.lastpayment()
        last_payment = datetime.fromisoformat(dt).date()

    cx = gnucash_connect(file)
    entries = gnucash_payments_since(cx, account_name, last_payment)
    operations = determine_operations(entries)

    (to_create, to_extend, to_check) = \
        sort_ops_into_buckets(medreg_session, operations)

    runner = DryOperations(medreg_session)

    if args.force:
        runner = RealOperations(medreg_session)

    runner.handle_extend_operations(to_extend)
    print("")
    runner.handle_create_operations(to_create)
    print("")
    runner.handle_check_operations(to_check)

    return 0


if __name__ == '__main__':
    sys.exit(main())
