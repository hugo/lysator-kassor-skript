/**
 * Ladda ner alla kontoutdrag från Nordeas redovisningssida från ett
 * givet datum t.o.m. idag.
 *
 * Körs i webbläsaren från JavaScript-konsollen.
 *
 * Se även nordea.py för en version som använder Selenium istället.
 */

const KONTO_LYSATOR = '8856692'
const KONTO_RUNEBERG = '9119983'

const REDOVISNING_URL = 'https://corporate.nordea.se/ndw-app-documents/?documentType=statementsandreports'

let last_accounting = new Date('2023-11-13')

for (let el of document.querySelectorAll('.ncc-table tbody tr')) {
	const datum = new Date(el.querySelector('[aria-label*="datum"').textContent.trim())
	const konto = el.querySelector('[aria-label*="avtalsnummer"').textContent.trim()
	const checkbox = el.querySelector('input[type="checkbox"]')

	if (konto === KONTO_LYSATOR && datum > last_accounting) {
		checkbox.click()
	}
}

const btn = document.querySelector('button[aria-label="knappen hämta dokument"]')
btn.click()
