#!/usr/bin/env python3

"""
"Automatiskt" laddar ner redovisning från Nordea.

Beror på följande Archlinuxpaket:
- geckodriver
- python-selenium
- selenium-manager
"""

# Nuvarande version är trasig, då den inte väntar på att sidorna är
# redo.

from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.by import By
from datetime import date

import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

import sys
import time

REDOVISNING_URL = 'https://corporate.nordea.se/ndw-app-documents/?documentType=statementsandreports'  # noqa: E501
KONTO_LYSATOR = '8856692'
KONTO_RUNEBERG = '9119983'

pnr = '19XXXXXXXXXX'


last_accounting = date.fromisoformat('2024-01-02')

options = FirefoxOptions()
options.add_argument('--safe-mode')
browser = webdriver.Firefox(options=options)
browser.get(REDOVISNING_URL)


def q(browser, selector):
    # return browser.find_element(By.CSS_SELECTOR, selector)
    # return WebDriverWait(browser, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))
    return WebDriverWait(browser, 20).until(EC.element_to_be_clickable((By.CSS_SELECTOR, selector)))


def qs(browser, selector):
    return browser.find_elements(By.CSS_SELECTOR, selector)


# Klicka bort kak-prompten
q(browser, '.wscrBannerContentInner a')
qs(browser, '.wscrBannerContentInner a')[1].click()

time.sleep(1)

# Klicka på logga-in knappen
q(browser, '.login-box button').click()

time.sleep(1)

# Välj ID-dosa som authentiseringsmetod
id = q(browser, '[aria-label="Nordea ID-dosa"]').get_attribute('id')
q(browser, f'label[for="{id}"]').click()

# Knappa in perssonnummer
q(browser, '#qrt-se-user-id').send_keys(pnr)

# "Logga in"
q(browser, '.auth-form button[type="submit"]').click()

# sys.exit(0)

# wait for location to become 'https://corporate.nordea.se/overview'
WebDriverWait(browser, 120).until(EC.url_to_be('https://corporate.nordea.se/overview'))

# then navigate to REDOVISNING_URL
browser.get(REDOVISNING_URL)

# Sidan kan behöva skrollas för att ladda in äldre dokument
for el in qs(browser, '.ncc-table tbody tr'):
    date_el = q(browser, '[aria-label*="datum"]')
    datum = date.fromisoformat(date_el.text.strip())
    konto = q(browser, '[aria-label*="avtalsnummer"').text.strip()
    checkbox = q(browser, 'input[type="checkbox"]')

    if konto == KONTO_LYSATOR and datum > last_accounting:
        checkbox.click()

# submit_btn = q(browser, 'button[aria-label="knappen hämta dokument"]')
# submit_btn.click()
