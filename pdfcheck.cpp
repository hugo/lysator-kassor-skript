#include <libgen.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include <filesystem>
#include <iostream>
#include <memory>
#include <regex>
#include <tuple>
#include <unordered_map>

#include <poppler-document.h>
#include <poppler-page.h>
#include <poppler-rectangle.h>
#include <poppler-global.h>

/** Trim leading and trailing whitespace from string. */
std::string trim(std::string in) {
	auto proc = [](unsigned char c) { return !std::isspace(c); };
	auto lpads = std::find_if(in.begin(), in.end(), proc) - in.begin();
	auto rpads = std::find_if(in.rbegin(), in.rend(), proc) - in.rbegin();
	return in.substr(lpads, in.length() - lpads - rpads);
}

/** Get date from pdf page */
std::string get_date(const poppler::page *page, int y) {
	poppler::rectf r(195, y, 72, 10);
	poppler::ustring msg = page->text(r);
	return trim(msg.to_latin1());
}

/** Find key key with the "minimum" value in unordered_map */
template<typename T, typename V>
T min_key(const std::unordered_map<T, V> &d) {
	auto it = d.begin();
	T minimi = it->first;
	it++;
	for (; it != d.end(); it++) {
		minimi = std::min(minimi, it->first);
	}
	return minimi;
}

/**
 * Checks if all parts of the interval are present.
 */
std::tuple<std::string, std::string, bool>
check_intervals(const std::vector<std::pair<std::string, std::string>> &intervals) {
	std::unordered_map<std::string, std::string> d;

	for (auto p : intervals) {
		d[p.first] = p.second;
	}

	std::string start = min_key(d), end;
	auto true_start = start;

	bool err = false;

	while (true) {
		if (d.contains(start)) {
			end = d[start];
			d.erase(start);
			start = end;
		} else {
			if (d.empty()) {
				break;
			} else {
				end = min_key(d);
				std::cout
					<< "!! Saknar dokument mellan "
					<< start << " och " << end << "\n";
				err = true;
				start = end;
			}
		}
	}

	return { true_start, start, err };
}

/** Find all Kontoudrag-files in the given directory */
std::vector<std::string> find_files(std::string path) {
	std::regex rx("KONTOUTDRAG-20[0-9][0-9]-[0-9][0-9]-[0-9][0-9].pdf");
	std::smatch m;

	std::vector<std::string> files;
	for (const auto &entry : std::filesystem::directory_iterator(path)) {
		auto s = entry.path().filename().string();
		if (std::regex_match(s, m, rx)) {
			files.push_back(entry.path());
		}
	}
	return files;
}

/** Merge pdf:s */
void pdfunite(std::string outfile, const std::vector<std::string> &files) {
	/**
	 * TODO this should ideally be rewritten to use the API,
	 * but merging PDFs is suprisingly cumbersome.
	 */
	int pid;
	switch ((pid = fork())) {
		case -1: /* fail */
			std::cerr << "Failed forking\n";
			return;

		case 0: /* child */
			{
				size_t len = files.size() + 3;
				auto argv = new char*[len];

				argv[0] = strdup("pdfunite");
				for (int i = 0; i < files.size(); i++) {
					argv[i + 1] = strdup(files[i].c_str());
				}
				argv[len - 2] = strdup(outfile.c_str());
				argv[len - 1] = NULL;

				execvp("pdfunite", argv);

				for (int i = 0; i < len - 1; i++) {
					free(argv[i]);
				}
				delete[] argv;

				break;
			}

		default: /* parent */
			waitpid(pid, NULL, 0);
			break;
	}
}

int xxx() {
	std::vector<std::pair<std::string, std::string>> intervals;

	auto files = find_files("/home/hugo/Downloads/");
	for (const auto &file : files) {
		auto doc = std::unique_ptr<poppler::document>(
				poppler::document::load_from_file(file));
		auto page = std::unique_ptr<poppler::page>(doc->create_page(0));

		auto end = get_date(page.get(), 212);
		auto start = get_date(page.get(), 236);
		intervals.push_back({start, end});
	}

	if (intervals.size() == 0) {
		std::cout << "Hittade inga kontoutdrag\n";
		return 0;
	}

	std::string start, end;
	bool err;

	std::tie(start, end, err) = check_intervals(intervals);

	if (! err) {
		std::cout << "Har komplett intervall "
			<< start << " till " << end
			<< std::endl;
		std::string outfile = "merged.pdf";
		std::sort(files.begin(), files.end());
		pdfunite(outfile, files);
		std::cout << "Slog ihop till " << outfile << std::endl;
	} else {
		std::cout
			<< "Har intervallet "
			<< start << " till " << end
			<< std::endl;
	}

	return 0;
}

int main() { xxx(); }
